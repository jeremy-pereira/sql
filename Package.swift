// swift-tools-version:5.3
import PackageDescription

#if os(Linux)
let package = Package(
    name: "SQL",
    products: [
        .library(name: "SQL", targets: ["SQL"]),
    ],
    dependencies: [
		.package(name: "Toolbox", url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/toolbox.git", from: "21.0.0"),
    ],
    targets: [
        .target(name: "SQL", dependencies: ["Toolbox", "CSQLite3"], linkerSettings: [.linkedLibrary("sqlite3")]),
        .target(name: "CSQLite3"),
        .testTarget(name: "SQLTests", dependencies: ["SQL"]),
    ]
)
#else
let package = Package(
    name: "SQL",
    platforms: [
        .macOS(.v10_15),
    ],
    products: [
        .library(name: "SQL", targets: ["SQL"]),
    ],
    dependencies: [
		.package(name: "Toolbox", url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/toolbox.git", from: "21.0.0"),
    ],
    targets: [
        .target(name: "SQL", dependencies: ["Toolbox"]),
        .testTarget(name: "SQLTests", dependencies: ["SQL"]),
    ]
)
#endif
