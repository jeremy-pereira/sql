# SQL Framework

## What is this repository for?

This framework provides a thin and hopefully extensible wrapper to a relational database. 

[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up?

### Getting set up

This is a Swift package. Add it to your project and you are good to go.

The framework makes use of the `Toolbox` package (also available from me as open source). 

Under Linux, this needs an extra dependency for SQLite3 integration. The dependency is pulled in automatically but it depends in turn on having the sqlite3 developer toolkit. If it complains about not having `sqlite3.h`, you need to get your SQLite developer package.

### Using the framework in your code.

It all starts with a `Connection` object. Instantiate a connection as the implementation of the database you want. Foe example:

    var myConnection: Connection = SQLite3Connection("somefile.sqlite3")

After that, you need to explicitly open the connection. 

    try myConnection.open()
    
Then, you can use the connection to prepare statements, run transactions etc. There's two ways to do each of these, either run the methods explicitly

    
    preparedStatement = try myConnection.prepareStatement(myStatement)
    try myConnection.beginTransaction()
    
    // Do stuff
    
    myStatement.finalise()
    try myConnection.commitTransaction()

Or you can use the closure versions of the same.

    try myConnection.transaction
    {
    try myConnect.with(statement: aStatement)
    	{
    		preparedStatement in
    		// Do stuff
    	}
    }

The second form automatically takes care of cleanup of statements and transactions. In particular, if you throw an error through from the transaction closure. the transaction will be aborted instead of committed.

## Releases

### 4.0

Many improvements to bump to Swift 5.3

### 2.0

- `select` where clause renamed from `whereClause` to `where`.

- `groupBy` clause added to `select`.
