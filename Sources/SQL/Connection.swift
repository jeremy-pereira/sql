//
//  Connection.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 16/10/2015.
//  Copyright © 2015, 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Toolbox

/// Interface that defines an SQL connection's functionality
public protocol Connection
{
    /// Open the database connection.
    /// - Throws: If the connection cannot be opened
	func open() throws

    /// Cloase the database connection
    func close()

    /// True if the database connection is open
    var isOpen: Bool { get }

    ///    Prepare a SQL statement for execution
    ///
    /// - parameter statement: SQL statement to prepare for execution
    ///
    /// - returns: A prepared statement
    /// - Throws: If the prepare fails for any reason
    func prepare(statement: Statement) throws -> PreparedStatement

    ///    Convenience function to prepare a statement, do some stuff with it
    ///    then finalise the statement.
    ///
    /// - parameter statement: SQL statement
    /// - parameter doBlock:   Block to execute. It takes a parameter which is
    ///                        the prepared statement. It can throw and any errors
    ///                        will be propagatedf by this method.
    ///
    /// - throws: If the prepare fails or the block throws an exception.
    ///
    func with<T>(statement: Statement, doBlock: (PreparedStatement) throws -> T) throws -> T

    ///    Create a SQL string from an SQL statement
    ///
    /// - parameter statement: Statement to convert to a string
    ///
    ///    - returns: A string containing a SQL query
    func stringFrom(statement: Statement) -> String

    ///    Executes a block in a transaction. Any exception thrownby the block
    ///    will cause a transaction roll back. If the block completes normally, 
    ///    it will be committed.
    ///
    /// - parameter block: The block to be executed in a transaction
    ///
    /// - throws: errors thrown in the blockare propagated to the caller.
    func transaction(block: () throws -> ()) throws

    ///    Test if the database contains the given table.
    ///
    /// - parameter table: Table to test for
    ///
    /// - throws: If an error occurs querying the metadata
    ///
    /// - returns: `true` if the table exists, `false` if not.
    func has(table: TableName) throws -> Bool

    ///    Begin a database transaction
    ///
    /// - throws: If the begin fails
    func beginTransaction() throws

    ///    Roll back a database transaction
    ///
    /// - throws: If the roll back fails
    func rollBackTransaction() throws

    ///    Commit a database transaction
    ///
    /// - throws: If the commit fails
    func commitTransaction() throws

    ///    If the database has a trace facility, turn it on or off
    ///
    /// - parameter on: Is the trace on or  not.
    func trace(on: Bool)
}

public extension Connection
{
	/// Prepare a statement and then execute a block of code with the statement
	///
	/// This is a convenience function that automatically finalises the block
	/// even if the block throws an exception.
	/// - Parameters:
	///   - statement: The statement to proepare
	///   - doBlock: The block of code to execute. The prepared statement is
	///              passed as a parameter.
	/// - Returns: The return from the block if there is one
	/// - Throws: If the block throws or if we fail to prepare the statement
    func with<T>(statement: Statement, doBlock: (PreparedStatement) throws -> T) throws -> T
    {
		let preparedStatement = try self.prepare(statement: statement)
        defer { preparedStatement.finalize() }
        return try doBlock(preparedStatement)
    }
}

///    Protocol  representing a prepared statement
public protocol PreparedStatement
{
    ///    Executes the prepared statement
    ///
    ///    - throws: If the execution fails
	func execute() throws

    /// The number of rows affected by the statement. The result is meaningful
    /// only for `UPDATE`, `INSERT` and `DELETE`. In a threaded program, you may 
    /// have to take care to ensure that this is correct.
    var affectedRowCount: Int { get }

    /// Bind a prepared statement to some values
    ///
    /// - parameter values: The values to bind
    ///
    /// - throws: If there is a binding error
    func bind(values: [SQLValue]) throws

	///    Does any clean up before throwing the statement away
    func finalize()

 	/// The statement's result set. The result set should be empty for statements
    /// that do not have results e.g. UPDATE and INSERT. That is to say
    /// `self.resultSet.next() == nil`.
    ///
    /// There is only one result set per statement and its `next()` method may 
    /// be destructive.
    var resultSet: ResultSet { get }
    
    ///    Resets a prepared statement so that it may be reused.
    ///
    ///    - throws: If the reset fails
    func reset() throws

}

///    Protocol for
public protocol ResultSet
{
    ///    Get the next row in the result set.
    ///
    /// - returns: The next row for the result set or nil if we are at the
    ///            end of the results.
    /// - throws: The method may throw an exceptionu if the underlying 
    ///           implementation encounters a problem (excluding end of data).
    func next() throws -> [ String : SQLValue ]?
}

