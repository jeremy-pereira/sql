//
//  Error.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 16/10/2015.
//  Copyright © 2015, 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

/// SQL errors
public enum Error: Swift.Error, CustomStringConvertible
{
    case openFailed(nativeError: Int, connectString: String)
    case implementation(explanation: String)
    case prepareFailed(nativeError: Int, statement: Statement)
    case executeFailed(nativeError: Int, text: String, statement: PreparedStatement)
    case bindError(nativeError: Int, bindType: String, index: Int)
    case unexpectedResult(message: String)
    case resetError(nativeError: Int)
    case connectionBind(entity: String)
    case typeMismatch(message: String)

    public var description: String
    {
		switch self
        {
        case .openFailed(let nativeError, let connectString):
            return "SQL Open failed, error \(nativeError), connect string: \(connectString)"
        case .implementation(let explanation):
            return "Implelentation error: \(explanation)"
        case .prepareFailed(let nativeError, _):
            return "SQL Prepare failed, error: \(nativeError)"
        case .executeFailed(let nativeError, let text, _):
            return "SQL execute failed, error: \(nativeError), text: \(text)"
        case .bindError(let nativeError, let bindType, let index):
            return "SQL bind failed, error: \(nativeError), bind type: \(bindType), index: \(index)"
        case .unexpectedResult(let message):
            return "SQL unexpected result: \(message)"
        case .resetError(let nativeError):
            return "Statement reset error: \(nativeError)"
        case .connectionBind(let entity):
            return "\(entity) not bound to a database connection"
        case .typeMismatch(let message):
            return "SQL type conversion failed: \(message)"
        }
    }
}
