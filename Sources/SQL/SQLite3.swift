//
//  SQLite3.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 18/10/2015.
//  Copyright © 2015, 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

#if os(Linux)
import CSQLite3
import SwiftGlibc
#else
import SQLite3
#endif

import Toolbox

private let log = Logger.getLogger("SQL.SQLite3")

/// Encapsulates a sqllite3 connection
public class SQLLite3Connection: Connection
{

    private let fileName: String
	fileprivate var dbConnection: OpaquePointer? = nil

    public init(fileName: String)
    {
        self.fileName = fileName
    }

    deinit
    {
        if isOpen
        {
            log.warn("Connection for \(fileName) was not explicitly closed")
            self.close()
        }
    }

    public func open() throws
    {
        guard !isOpen else { return }

        log.debug("Opening connection \(fileName)")
		var fileNameBytes = try fileName.signedBytes(encoding: String.Encoding.utf8)
        fileNameBytes.append(0)
        let result = sqlite3_open(&fileNameBytes, &dbConnection)
        guard result == SQLITE_OK
            else
        {
            sqlite3_close(dbConnection)
            throw SQL.Error.openFailed(nativeError: Int(result), connectString: fileName)
        }
        _isOpen = true
        //
        // Turn on foreign key support
        //
		let turnOnFK = try self.prepare(statement: Statement.pragma("foreign_keys = ON"))
        defer { turnOnFK.finalize() }
		try turnOnFK.execute()
    }

    public func close()
    {
        if isOpen
        {
            sqlite3_close(dbConnection)
            _isOpen = false
        }
    }

    private var _isOpen = false
    public var isOpen: Bool { return _isOpen }

    public func has(table: TableName) throws -> Bool
    {
        let masterName = TableName(schema: table.schema, "sqlite_master")
        let select = Statement.select(columnNames: ["COUNT(*)"],
            								 from: masterName,
                                      		where: SQLBooleanExpression.relation("name", .eq, SQLValue.varChar(table.name)),
                                      conversions: [:])
        var ret = false
		try self.with(statement: select)
        {
			try $0.execute()
			let results = $0.resultSet
			guard let firstRow = try results.next()
                else
                {
                    throw SQL.Error.unexpectedResult(message: "Selecting count should return a row")
                }
            guard let count = firstRow["COUNT(*)"]
            else
            {
                throw SQL.Error.unexpectedResult(message: "No count returned")
            }
			switch count
            {
            case SQLValue.integer(let intCount):
                ret = intCount > 0
            default:
                throw SQL.Error.unexpectedResult(message: "Count is not an integer")
            }
        }
		return ret
    }


	/// Create a SQL string from the statement suitable for this connection
	/// - Parameter statement: The statement from which to create a string
	/// - Returns: The statement string
    public func stringFrom(statement: Statement) -> String
    {
        //log.debug("Stringified \(statement)")
        var ret: String
        switch statement
        {
        case .create(let tableName, let columns, let constraints):
			ret = "CREATE TABLE "
            ret += tableName.asString + " "
            var firstCol = true
			for column in columns
            {
				if firstCol
                {
                    ret += "("
                    firstCol = false
                }
                else
                {
                    ret += ","
                }
				ret += makeStringFromColumn(column: column)
            }
            if constraints.count > 0
            {
                ret += ", "
				let constraintStrings = constraints.map{ makeStringFrom(constraint: $0) }
				ret += constraintStrings.joined(separator: ", ")
            }
            ret += ")"
        case .insert(let tableName, let columns):
            ret = "INSERT INTO "
            ret += tableName.asString
            var firstCol = true
            var valuePlaceHolders = ""
            for column in columns
            {
                if firstCol
                {
                    ret += "("
                    valuePlaceHolders += "(?"
                    firstCol = false
                }
                else
                {
                    ret += ","
                    valuePlaceHolders += ",?"
                }
                ret += column
            }
            valuePlaceHolders += ")"
            ret += ") values" + valuePlaceHolders
        case .select(let columnNames, let tableName, let whereClause, let groupBy, _):
            //           log.debug("Stringified \(statement)")
            ret = "SELECT "
			ret += columnNames.joined(separator: ",")
            ret += " FROM "
            ret += tableName.asString
            if let whereClause = whereClause
            {
				ret += " WHERE " + makeStringFrom(booleanExpression: whereClause)
            }
			if groupBy.count > 0
			{
				ret += " GROUP BY "
				ret += groupBy.joined(separator: ",")

			}
        case .dropTable(let tableName):
            ret = "DROP TABLE " + tableName.asString
        case .update(let tableName, let columns, let whereClause):
            ret = "UPDATE " + tableName.asString + " SET "
			ret += columns.map({ $0 + " = ?" }).joined(separator: ", ")
            if let whereClause = whereClause
            {
				ret += " WHERE " + makeStringFrom(booleanExpression: whereClause)
            }
        case .delete(let tableName, let whereClause):
            ret = "DELETE FROM " + tableName.asString
            if let whereClause = whereClause
            {
				ret += " WHERE " + makeStringFrom(booleanExpression: whereClause)
            }
        case .beginTransaction:
            ret = "BEGIN TRANSACTION"
        case .commitTransaction:
            ret = "COMMIT TRANSACTION"
        case .rollbackTransaction:
            ret = "ROLLBACK TRANSACTION"
        case .pragma(let pragma):
            ret = "PRAGMA " + pragma
        }
        //       log.debug("Stringified \(statement)")
        //        log.debug("to '\(ret)\'")
        return ret
    }

    private func makeStringFromColumn(column: Column) -> String
    {
		var ret = column.name + " "
		ret += makeStringFrom(sqlType: column.sqlType)
		for constraint in column.constraints
        {
			ret += " " + makeStringFrom(constraint: constraint)
        }
		return ret
    }

    private func makeStringFrom(sqlType: SQLType) -> String
    {
        let ret: String
        switch sqlType
        {
        case .integer:
            ret = "INTEGER"
        case .char, .varChar, .date, .text:
            ret = "TEXT"
		case .float:
			ret = "REAL"
		}
        return ret
    }

    private func makeStringFrom(constraint: Column.Constraint) -> String
    {
        var ret = ""
        switch constraint
        {
        case .`default`(let value):
            ret = "DEFAULT " + value.asLiteral
        case .primaryKey(let autoIncrement):
            ret = "PRIMARY KEY" + (autoIncrement ? " AUTOINCREMENT" : "")
        case .unique:
			ret = "UNIQUE"
        case .notNull:
            ret = "NOT NULL"
        }
        return ret
    }

    private func makeStringFrom(constraint: Constraint) -> String
    {
        var ret = ""
        switch constraint
        {
        case Constraint.foreignKey(let columns, let referenceTable, let foreignColumns):
			let joinedColumns = columns.joined(separator: ", ")
			let joinedForeignColumns = foreignColumns.joined(separator: ", ")
			ret = "FOREIGN KEY (" + joinedColumns
				+ ") REFERENCES " + referenceTable.asString + " (" + joinedForeignColumns + ")"

        }
        return ret
    }


    private func makeStringFrom(booleanExpression expression: SQLBooleanExpression) -> String
    {
        let ret: String
		switch expression
        {
        case .relation(let column, let relation, let value):
			ret = column + " " + relation.asString + " " + value.asLiteral
        case .or(let left, let right):
			ret = "(" + makeStringFrom(booleanExpression: left) + ") OR (" + makeStringFrom(booleanExpression: right) + ")"
        case .and(let left, let right):
			ret = "(" + makeStringFrom(booleanExpression: left) + ") AND (" + makeStringFrom(booleanExpression: right) + ")"
        case .not(let expr):
			ret = "NOT (" + makeStringFrom(booleanExpression: expr) + ")"
       case .isNull(let column):
            ret = column + " IS NULL"
        case .isNotNull(let column):
            ret = column + " IS NOT NULL"
        }
        return ret
    }

    public func prepare(statement: Statement) throws -> PreparedStatement
    {
		let statementString = stringFrom(statement: statement)
        var statementBytes = try! statementString.signedBytes(encoding: String.Encoding.utf8)
        log.debug("About to prepare \(statementString)")
        var sql3PreparedStatement: OpaquePointer? = nil
		let result = sqlite3_prepare_v2(dbConnection, &statementBytes, Int32(statementBytes.count), &sql3PreparedStatement, nil)
        guard result == SQLITE_OK else
        {
			log.error("Prepare failed with error \(result), statement is \"\(self.stringFrom(statement: statement))\"")
            throw SQL.Error.prepareFailed(nativeError: Int(result), statement: statement)
        }
        let ret: SQLite3PreparedStatement
        switch statement
        {
        case .select(let columnNames, _, _, _, let conversions):
            ret = SQLite3PreparedSelect(sql3PreparedStatement!, columns:  columnNames, conversions: conversions, connection: self)
        default:
            ret = SQLite3PreparedStatement(sql3PreparedStatement!, connection: self)
        }
		return ret
    }

    public func transaction(block: () throws -> ()) throws
    {
        try beginTransaction()
        do
        {
            try block()
            try commitTransaction()
        }
        catch
        {
            try rollBackTransaction()
            throw error
        }
    }

    public func beginTransaction() throws
    {
		let preparedBegin = try self.prepare(statement: Statement.beginTransaction)
        defer { preparedBegin.finalize() }
		try preparedBegin.execute()
    }

    public func rollBackTransaction() throws
    {
		let preparedRollBack = try self.prepare(statement: Statement.rollbackTransaction)
        defer { preparedRollBack.finalize() }
        try preparedRollBack.execute()
    }

    public func commitTransaction() throws
    {
		let preparedCommit = try self.prepare(statement: Statement.commitTransaction)
        defer { preparedCommit.finalize() }
        try preparedCommit.execute()
    }

	/// Flags usable in a trace call
	public struct TraceFlags: OptionSet
	{
		public var rawValue: UInt32
		public init(rawValue: UInt32)
		{
			self.rawValue = rawValue
		}

		public init(_ int32: Int32)
		{
			self.init(rawValue: UInt32(int32))
		}
		/// Trace row operations
		public static let row = TraceFlags(SQLITE_TRACE_ROW)
		/// Trace prepared statement in itiation
		public static let statement = TraceFlags(SQLITE_TRACE_STMT)
		/// Profile a prepared statement
		public static let profile = TraceFlags(SQLITE_TRACE_PROFILE)
		/// Trace database closes
		public static let close = TraceFlags(SQLITE_TRACE_CLOSE)
		/// No tracing at all
		public static let none = TraceFlags(0)
	}

	// TODO: A more generic traqce function
    public func trace(on: Bool)
    {
        if on
        {
            sqlite3_trace_v2(dbConnection,
							 TraceFlags.statement.rawValue,
			{ (traceFlags, userData, p, x) -> Int32 in
				guard TraceFlags(rawValue: traceFlags).contains(TraceFlags.statement)
					else { return 0 }
				var sqlStatement: String?
				if let x = x
				{
					// This is probably really bad. I should really count the
					// bytes to the null and use that as the capacity.
					let typedX = x.bindMemory(to: UInt8.self, capacity: 1)
					let xAsString = String(cString: typedX)
					if xAsString.hasPrefix("--")
					{
						sqlStatement = xAsString
					}
				}
				if sqlStatement == nil
				{
					let pAsCString = sqlite3_expanded_sql(OpaquePointer(p))
					sqlStatement = String(cString: pAsCString!)
				}
                log.debug("SQLITE3 TRACE: \(sqlStatement!)")
				return 0
			}, nil)
        }
    }
}

private class SQLite3PreparedStatement: PreparedStatement, ResultSet
{
    fileprivate var sql3PreparedStatement: OpaquePointer
    private var finalized = false
    fileprivate weak var connection: SQLLite3Connection!

    init(_ sql3PreparedStatement: OpaquePointer, connection: SQLLite3Connection)
    {
		self.sql3PreparedStatement = sql3PreparedStatement
        self.connection = connection
    }

    deinit
    {
        if !finalized
        {
            log.warn("prepared statemtent was not explicitly finalized")
            self.finalize()
        }
    }

	fileprivate func finalize()
    {
        if !finalized
        {
            log.debug("Finalising \(self)")
            sqlite3_finalize(sql3PreparedStatement)
            boundTextValues.removeAll()
            finalized = true
        }
    }

	fileprivate func execute() throws
    {
        log.debug("Executing \(self)")
        let result = sqlite3_step(sql3PreparedStatement)
        switch result
        {
        case SQLITE_DONE:
            affectedRowCount = Int(sqlite3_changes(connection.dbConnection))
            break
        case SQLITE_ROW:
            fatalError("Wasn't expecting a non select to give SQLITE_ROW as a result")
        default:
            let text = String(cString: sqlite3_errmsg(connection.dbConnection), encoding: String.Encoding.utf8)!
            throw Error.executeFailed(nativeError: Int(result), text: text, statement: self)
        }
    }

	fileprivate var affectedRowCount: Int = 0

	fileprivate func bind(values: [SQLValue]) throws
    {
        var bindIndex: Int32 = 1
        for value in values
        {
            log.debug("Binding \(value)")
			switch value
            {
            case .varChar(let string):
				try bindText(value: string, index: bindIndex)
            case .char(let string):
				try bindText(value: string, index: bindIndex)
            case .integer(let intValue):
				try bindInteger(value: intValue, index: bindIndex)
            case .float(let floatValue):
				try bindFloat(value: floatValue, index: bindIndex)
            case .date(let date):
				try bindText(value: date.isoString, index: bindIndex)
            case .null:
                break
//                let result = sqlite3_bind_null(sql3PreparedStatement, bindIndex)
//                if result != SQLITE_OK
//                {
//                    throw Error.BindError(nativeError: Int(result), bindType: "null", index: Int(bindIndex))
//                }
            case .placeHolder:
                throw Error.bindError(nativeError: 0, bindType: "Placeholder", index: Int(bindIndex))
            }
            bindIndex += 1
        }
    }

    private var boundTextValues: [[Int8]] = []

    private func bindText(value: String, index: Int32) throws
    {
        // ((UnsafeMutablePointer<Void>) -> Void)!
        let valueBytes = try value.signedBytes(encoding: String.Encoding.utf8)
        let boundValueIndex = boundTextValues.count
        boundTextValues.append(valueBytes)

        let result = sqlite3_bind_text(sql3PreparedStatement, index, boundTextValues[boundValueIndex], Int32(valueBytes.count), {
            thePointer in
            log.debug("Finished with a text bind")
        })
        if result != SQLITE_OK
        {
            throw Error.bindError(nativeError: Int(result), bindType: "text", index: Int(index))
        }
    }

    private func bindInteger(value: Int, index: Int32) throws
    {
		let result = sqlite3_bind_int64(sql3PreparedStatement, index, Int64(value))
        if result != SQLITE_OK
        {
            throw Error.bindError(nativeError: Int(result), bindType: "Integer", index: Int(index))
        }

    }

    private func bindFloat(value: Double, index: Int32) throws
    {
        let result = sqlite3_bind_double(sql3PreparedStatement, index, value)
        if result != SQLITE_OK
        {
            throw Error.bindError(nativeError: Int(result), bindType: "Integer", index: Int(index))
        }
        
    }

	fileprivate var resultSet: ResultSet
    {
        return self
    }

	fileprivate func next() throws -> [String : SQLValue]?
    {
        return nil
    }

	fileprivate func reset() throws
    {
        let result = sqlite3_reset(sql3PreparedStatement)
        if result != SQLITE_OK
        {
            throw Error.resetError(nativeError: Int(result))
        }
    }
}

private class SQLite3PreparedSelect: SQLite3PreparedStatement
{
    private let conversions: [ String : (SQLValue) throws -> SQLValue]
    private let columns: [String]

    init(_ sql3PreparedStatement: OpaquePointer,
           				 columns: [String],
                     conversions: [ String : (SQLValue) throws -> SQLValue],
                      connection: SQLLite3Connection)
    {
        self.columns = columns
        self.conversions = conversions
        super.init(sql3PreparedStatement, connection: connection)
    }

    private var rowAvailable = false

	fileprivate override func execute() throws
    {
        log.debug("Executing \(self)")
        let result = sqlite3_step(sql3PreparedStatement)
        switch result
        {
        case SQLITE_DONE:
            break
        case SQLITE_ROW:
            rowAvailable = true
        default:
            let text = String(cString: sqlite3_errmsg(connection.dbConnection), encoding: String.Encoding.utf8)!
            throw Error.executeFailed(nativeError: Int(result), text: text, statement: self)
        }
    }

	fileprivate override func next() throws -> [String : SQLValue]?
    {
        guard rowAvailable else { return nil }

        var ret: [String : SQLValue] = [:]

        let columnCount = sqlite3_column_count(sql3PreparedStatement)

        for i in Int32(0) ..< Int32(columnCount)
        {
            let name = columns[Int(i)]
            let columnType = sqlite3_column_type(sql3PreparedStatement, i)
            let value: SQLValue
            switch columnType
            {
            case SQLITE_INTEGER:
                value = SQLValue.integer(Int(sqlite3_column_int64(sql3PreparedStatement, i)))
            case SQLITE_FLOAT:
                value = SQLValue.float(sqlite3_column_double(sql3PreparedStatement, i))
            case SQLITE_TEXT:
				value = SQLValue.varChar(String(cString: sqlite3_column_text(sql3PreparedStatement, i)))
            case SQLITE_NULL:
                value = SQLValue.null
            default:
                fatalError("Unsupported SQLite3 type \(columnType)")
            }
            if let conversion = conversions[name]
            {
                ret[name] = try conversion(value)
            }
            else
            {
                ret[name] = value
            }
        }


        // Get ready for the next row
        rowAvailable = false
        try self.execute()
        
        return ret
    }
}
