//
//  Statement.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 18/10/2015.
//  Copyright © 2015, 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Toolbox

public extension String
{
    var sqlEscaped: String
    {
        // Ah, little Bobby Tables
		return self.reduce("")
		{
			if $1 == "'"
			{
				return $0 + "''"
			}
			else
			{
				return $0 + [$1] // Can you believe there is no way to append a character to a string
			}
		}
     }
}

public protocol Joinable
{
    var asString: String { get }
    func join(right: TableName, leftColumn: String, op: SQLRelationalOp, rightColumn: String) -> Joinable
}

public extension Joinable
{
    func join(right: TableName, leftColumn: String, op: SQLRelationalOp, rightColumn: String) -> Joinable
    {
        return Join(left: self, right: right, leftColumn: leftColumn, relation: op, rightColumn: rightColumn)
    }
}

///    Encapsulates a table name with or without a schema.
public struct TableName: Joinable
{
    private let _schema: String?
    private let _name: String

    public init(schema: String? = nil, _ name: String)
    {
        self._schema = schema
        self._name = name
    }

    /// The schema
    public var schema: String? { return _schema }
    /// The name within the schema
    public var name: String { return _name }

    /// Return the table name as a string. If it has a schema the result is in
    /// the form `schema.name` otherwise it is in the form `name`.
    public var asString: String
    {
        return (schema == nil ? "" : schema! + ".") + name
    }
}

public struct Join: Joinable
{
    private let left: Joinable
    private let right: TableName
    private let leftColumn: String
    private let rightColumn: String
    private let relation: SQLRelationalOp

    public init(left: Joinable, right: TableName, leftColumn: String, relation: SQLRelationalOp, rightColumn: String)
    {
        self.left = left
        self.right = right
        self.leftColumn = leftColumn
        self.rightColumn = rightColumn
        self.relation = relation
    }

    public var asString: String
    {
		return left.asString + " JOIN " + right.asString + " ON " + leftColumn + " " + relation.asString + " " + rightColumn
    }
}

///   SQL types
///
///   These are used in DDL statements to define the types of database columns.
///
///    - Integer: SQL integer type
///    - Char:    SQL fixed width char string type
///    - VarChar: SQL variable width char type
///    - Date:    Date object
///    - Text:    Unlimited length text object
public enum SQLType
{
	/// Integer type.
	///
	/// Should be mapped to a signed type of at least 64 bits in the database.
	case integer
	/// Floating point type
	///
	/// Should be mapped to a floating point type that can contain an ISO
	/// 8 byte double precision value.
	case float
	/// fixed width character string data.
	///
	/// Should be mapped to the `CHAR` type or equivalent. Note that, this
	/// framework assumes that the relevant type supports UTF-8.
    case char(Int)
	/// Variable character string data.
	///
	/// Should be mapped to the `VARCHAR` type or equivalent. Note that, this
	/// framework assumes that the relevant type supports UTF-8.
    case varChar(Int)
	/// A date
	///
	/// This type maps Swift `Date` to the supported date type for the database.
    case date
	/// Text data
	///
	/// Another alternative for string mapping. Again, should support UTF-8.
	case text

	/// Convenience property to determine if this is a date type
    public var isDate: Bool
    {
		switch self
        {
        case .date:
            return true
        default:
            return false
        }
    }
}

/// SQL Values
///
/// A typesafe way of dealing with values from SQL statements.
public enum SQLValue
{
	/// A 64 bit signed integer
	case integer(Int)
	/// An ISO floating point number.
	///
	/// The SQL representation is implementation dependent but probably the
	/// 643 bit double precision version.
    case float(Double)
	/// Fixed width string of UTF-8 code points.
    case char(String)
		/// Variable width string of UTF-8 code points.
    case varChar(String)
	/// A date
    case date(Date)
	/// Null value
    case null
	/// Placeholder used in SQL statements for values that will be bound later.
    case placeHolder

    /// Literal that can be used in a SQL statement
    var asLiteral: String
    {
		switch self
    	{
        case .integer(let value):
            return "\(value)"
        case .float(let value):
            return "\(value)"
        case .char(let value):
            return "'\(value.sqlEscaped)'"
        case .varChar(let value):
            return "'\(value.sqlEscaped)'"
        case .date(let date):
            return "'\(date.isoString)'"
        case .null:
            return "NULL"
        case .placeHolder:
            return "?"
        }
    }

    /// Convenience property that gives us the value as a string or nil
    public var asString: String?
    {
        switch self
        {
        case .integer(let value):
            return "\(value)"
        case .float(let value):
            return "\(value)"
        case .char(let value):
            return value
        case .varChar(let value):
            return value
        case .null:
            return nil
        case .date(let date):
            return date.isoString
        case .placeHolder:
            return nil
        }
    }
    /// Convenience property that gives us the value as an int or nil
    public var asInt: Int?
    {
        switch self
        {
        case .integer(let value):
            return value
        case .float(let value):
            return Int(value)
        case .char:
            return nil
        case .varChar:
            return nil
        case .date:
            return nil
        case .null:
            return nil
        case .placeHolder:
            return nil
        }
    }

    /// Convenience property that gives us the value as an int or nil
    public var asDate: Date?
    {
        switch self
        {
        case .integer(let value):
            return Toolbox.dateFrom(unixTime: value)
        case .float:
            return nil
        case .char:
            return nil
        case .varChar(let value):
            return Toolbox.dateFrom(isoString: value)
        case .date(let date):
            return date
        case .null:
            return nil
        case .placeHolder:
            return nil
        }
    }

	public func toSQLDate() throws -> SQLValue
    {
        if case SQLValue.null = self
        {
			return self
        }
        guard let theDate = self.asDate
        else
        {
			throw Error.typeMismatch(message: "Converting non date to date")
        }
        return SQLValue.date(theDate)
    }
}

public enum SQLRelationalOp
{
    case less
    case leq
    case eq
    case geq
	case greater
	case neq

    var asString: String
    {
        switch self
        {
        case .less:
            return "<"
        case .leq:
            return "<="
        case .eq:
            return "="
        case .geq:
            return ">="
        case .greater:
            return ">"
		case .neq:
			return "<>"
		}
    }
}

public indirect enum SQLBooleanExpression
{
    case relation(String, SQLRelationalOp, SQLValue)
    case and(SQLBooleanExpression, SQLBooleanExpression)
    case or(SQLBooleanExpression, SQLBooleanExpression)
    case not(SQLBooleanExpression)
	case isNull(String)
    case isNotNull(String)
}

public func == (column: String, value: SQLValue) -> SQLBooleanExpression
{
    return SQLBooleanExpression.relation(column, SQLRelationalOp.eq, value)
}

public func >= (column: String, value: SQLValue) -> SQLBooleanExpression
{
    return SQLBooleanExpression.relation(column, SQLRelationalOp.geq, value)
}

public func < (column: String, value: SQLValue) -> SQLBooleanExpression
{
    return SQLBooleanExpression.relation(column, SQLRelationalOp.less, value)
}

public func && (left: SQLBooleanExpression, right: SQLBooleanExpression) -> SQLBooleanExpression
{
    return (SQLBooleanExpression.and(left, right))
}

public func || (left: SQLBooleanExpression, right: SQLBooleanExpression) -> SQLBooleanExpression
{
    return (SQLBooleanExpression.or(left, right))
}

///    Defines a column for a create statement
public struct Column
{
    ///    Column constraints
    ///
    ///    - PrimaryKey: The column is the primary key
    ///    - Unique:     Values in the column must be unique
    ///    - Default:    The default value for the column
    ///    - NotNull:    The column may not be null
    public enum Constraint
    {
        ///    Makes the column the primary key
        ///
        /// - Parameter autoIncrement: true if the column is an autoincrement
        case primaryKey(autoIncrement: Bool)

        ///    Makes the column unique
        ///
        case unique

        ///    Supplies the default value for the column
        ///
        /// - Parameter SQLValue: Default value
        case `default`(SQLValue)
        ///    Makes the column a not null column
        ///
        case notNull
    }

    public init(name: String, sqlType: SQLType, constraints: [Constraint])
    {
        self.name = name
        self.sqlType = sqlType
        self.constraints = constraints
    }

    /// The name of the column
    public let name: String
    /// The type of the column
    public let sqlType: SQLType
    /// Any column constraints
    public let constraints: [Constraint]
}

public enum Constraint
{
    case foreignKey(columns: [String], referenceTable: TableName, foreignColumns: [String])
}

/// SQL statements
///
/// Use one of these to perform actions on the database. You first create a
/// statement of the appropriate type, then use the database connection to
/// prepare the statement and then bind values where needed. Finally, you caqn
/// execute the preppared statement.
///
/// An example using a simple `insert`
/// ```
/// let insert = Statement.insert(table: TableName("foo"), columnNames: ["name"])
/// do
/// {
///		let preparedInsert = try testConnection.prepare(statement: insert)
///		defer { preparedInsert.finalize() }
///		try preparedInsert.bind(values: [SQLValue.varChar("bar")])
///		try preparedInsert.execute()
/// }
/// catch
/// {
///		// Failure handling
/// }
/// ```

public enum Statement
{
	/// Create a new database table
    case create(table: TableName, columns: [Column], constraints: [Constraint])
	/// Insert a row into a database table
	///
	/// Column values are always set through binding the prepared statement, so
	/// they are not specified here.
    case insert(table: TableName, columnNames: [String])
	/// A select statement.
    case select(columnNames: [String],
        			   from: Joinable,
					  where: SQLBooleanExpression? = nil,
					groupBy: [String] = [],
		conversions: [String : ((SQLValue) throws -> SQLValue)] = [:])
	/// Drop a table from the database
    case dropTable(TableName)
	/// Update a row in the database.
	///
	/// Column values are always bound through the
	/// prepared statement, so only the names are specified here.
    case update(table: TableName, columns: [String], whereClause: SQLBooleanExpression?)
	/// Delete a set of rows from a table.
    case delete(from: TableName, whereClause: SQLBooleanExpression?)
	/// Begin a transaction
    case beginTransaction
	/// Commit a transaction
    case commitTransaction
	/// Rollboack a transaction
    case rollbackTransaction
	/// Perform an implementation specific pragma command.
    case pragma(String)
}
