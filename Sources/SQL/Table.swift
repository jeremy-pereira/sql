//
//  Table.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 04/11/2015.
//  Copyright © 2015, 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

///    Protocol for an object that we can run select queries on. This would 
///    include tables, joins and views (if supported).
public protocol CanBeJoined
{
    ///    Selects rows from the object according to the given criteria
    ///
    /// - parameter columns:     List of columns to select
    /// - parameter whereClause: Criteria for selection, if nil, select everything
    /// - parameter conversions: A dictionary of mappings from column names to
    ///                          functions that cast the result to a different
    ///                          type.
    /// - parameter resultBlock: Block to execute for each result that is found.
    /// - throws: If we fail to do the select or the table is not bound to a connection
    func select(columns: [String],
			whereClause: SQLBooleanExpression?,
			conversions: [ String : (SQLValue) throws -> SQLValue],
			resultBlock: ([String : SQLValue]) throws -> ()) throws

    ///    Join to another table
    ///
    /// - parameter rightTable:  Other table to join
    /// - parameter leftTable:   The table from which the left column comes
    /// - parameter leftColumn:  The left column of the relation
    /// - parameter rightColumn: The right column of the relation
    ///
    /// - returns: A join of this and the right table
    func join(_ rightTable: Table, leftTable: Table, leftColumn: String, rightColumn: String) -> CanBeJoined

    /// Database connection associated with the object
    var connection: Connection? { get set }

    /// Return a Joinable for this object
    var joinClause: Joinable { get }

}

public extension CanBeJoined
{
    func join(_ rightTable: Table, leftTable: Table, leftColumn: String, rightColumn: String) -> CanBeJoined
    {
        return JoinTable(left: self, right: rightTable, leftTable: leftTable, leftColumn: leftColumn, rightColumn: rightColumn)
    }
}

public struct Table: CanBeJoined
{
    /// The name of this table
    public let name: TableName

    public var connection: Connection?

    public init(tableName: TableName, connection: Connection? = nil)
    {
        self.name = tableName
        self.connection = connection
    }

    public init(_ name: String, schema: String? = nil)
    {
        self.init(tableName: TableName(schema: schema, name))
    }

    ///    Create a table
    ///
    /// - parameter columns:     Columns in the table
    /// - parameter constraints: Table constraints
    ///
    /// - throws: If we fail to reate the table or it is not bound to a connection
    public func create(columns: [Column], constraints: [Constraint]) throws
    {
        guard let connection = connection
        else
        {
            throw Error.connectionBind(entity: self.name.asString)
        }
        let statement = Statement.create(table: self.name,
                                       columns: columns,
                                   constraints: constraints)
		try connection.with(statement: statement)
        {
			try $0.execute()
        }
    }

    public func drop() throws
    {
        guard let connection = connection
        else
        {
            throw Error.connectionBind(entity: self.name.asString)
        }
        let dropStatement = Statement.dropTable(self.name)
		try connection.with(statement: dropStatement)
        {
            try $0.execute()
        }
    }

    ///    Insert a row into the table.
    ///
    /// - parameter columnValues: A dictionary of column names and values
    ///
    /// - throws: If we fail to insert or the table is not bound to a connection
    public func insert(columnValues: [String : SQLValue]) throws
    {
        guard let connection = connection
        else
        {
            throw Error.connectionBind(entity: self.name.asString)
        }
        let columnNames = Array(columnValues.keys)
        let valueArray = columnNames.map{ columnValues[$0]! }
        let insertStatement = Statement.insert(table: self.name, columnNames: columnNames)
		try connection.with(statement: insertStatement)
        {
			preparedInsert in
			try preparedInsert.bind(values: valueArray)
            try preparedInsert.execute()
        }
    }

    ///    Selects rows from the table according to the given criteria
    ///
    /// - parameter columns:     List of columns to select
    /// - parameter whereClause: Criteria for selection, if nil, select everything
    /// - parameter conversions: A dictionary of mappings from column names to
    ///                          functions that cast the result to a different 
    ///                          type. Values need not be represented if you do
    ///							 not mind what type you get back.
    /// - parameter resultBlock: Block to execute for each result that is found.
    /// - throws: If we fail to do the select or the table is not bound to a connection
    public func select(columns: [String],
				   whereClause: SQLBooleanExpression?,
				   conversions: [ String : (SQLValue) throws -> SQLValue],
                   resultBlock: ([String : SQLValue]) throws -> ()) throws
    {
        guard let connection = connection
        else
        {
            throw Error.connectionBind(entity: self.name.asString)
        }
		let selectStatement = Statement.select(columnNames: columns,
            										  from: self.name,
                                               		 where: whereClause,
                                               conversions: conversions)
		let preparedSelect = try connection.prepare(statement: selectStatement)
        defer { preparedSelect.finalize() }
        try preparedSelect.execute()
		let resultSet = preparedSelect.resultSet
        while let result = try resultSet.next()
        {
            try resultBlock(result)
        }
    }

    ///    Update rows according to the where clause
    ///
    /// - parameter columnValues: Dictionary of column names and new values
    /// - parameter whereClause:  Expression telling us which rows to update
    /// - returns: The number of affected rows.
    /// - throws: If we have no connection or the update fails
    public func update(columnValues: [String : SQLValue],
						whereClause: SQLBooleanExpression) throws -> Int
    {
        guard let connection = connection
            else
        {
            throw Error.connectionBind(entity: self.name.asString)
        }
        var ret = 0
        let columnNames = Array(columnValues.keys)
        let updateStatement = Statement.update(table: self.name,
            								 columns: columnNames,
                                         whereClause: whereClause)
        let values = columnNames.map{ columnValues[$0]! }
		try connection.with(statement: updateStatement)
        {
			preparedUpdate in
			try preparedUpdate.bind(values: values)
            try preparedUpdate.execute()
            ret = preparedUpdate.affectedRowCount
        }
        return ret
    }

    /// Delete rows from the table
    /// - Parameter whereClause: A where clause indicating which rows to delete
    /// - Returns: The number of rows deleted.
    /// - Throws: If the delete fails
    public func delete(whereClause: SQLBooleanExpression) throws -> Int
    {
        guard let connection = connection
            else
        {
            throw Error.connectionBind(entity: self.name.asString)
        }
        var rowCount = 0
        let deleteStatement = Statement.delete(from: self.name, whereClause: whereClause)
		try connection.with(statement: deleteStatement)
        {
            preparedDelete in
            try preparedDelete.execute()
            rowCount = preparedDelete.affectedRowCount
        }
        return rowCount
    }

    public var joinClause: Joinable { return self.name }
}

///    Encapsulates a join between two or more tables
fileprivate struct JoinTable: CanBeJoined
{
    private let left: CanBeJoined
    private let right: Table
    private let leftTable: Table
    private let leftColumn: String
    private let rightColumn: String

	fileprivate var connection: Connection?

    fileprivate init(left: CanBeJoined, right: Table, leftTable: Table, leftColumn: String, rightColumn: String)
    {
        self.left = left
        self.right = right
        self.leftTable = leftTable
        self.leftColumn = leftColumn
        self.rightColumn = rightColumn
    }

	fileprivate func select(columns: [String],
        					whereClause: SQLBooleanExpression?,
        					conversions: [ String : (SQLValue) throws -> SQLValue],
                            resultBlock: ([String : SQLValue]) throws -> ()) throws
    {
        guard let connection = connection
        else
        {
            throw Error.connectionBind(entity: self.joinClause.asString)
        }
        let selectStatement = Statement.select(columnNames: columns,
            										  from: self.joinClause,
                                               		 where: whereClause,
                                               conversions: conversions)
		let preparedSelect = try connection.prepare(statement: selectStatement)
        defer { preparedSelect.finalize() }
        try preparedSelect.execute()
        let resultSet = preparedSelect.resultSet
        while let result = try resultSet.next()
        {
            try resultBlock(result)
        }

    }

	fileprivate var joinClause: Joinable
    {
        return Join(left: left.joinClause,
                   right: right.name,
              leftColumn: leftTable.name.asString + "." + leftColumn,
            	relation: SQLRelationalOp.eq,
             rightColumn: right.name.asString + "." + rightColumn)
    }
}
