//
//  NewModelTests.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 03/11/2015.
//  Copyright © 2015, 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import SQL
import Toolbox

private let dbFile = "test.sqlite3"

private let log = Logger.getLogger("SQLTests.SQLTests")

class NewModelTests: XCTestCase
{
    var testConnection: SQL.Connection!

    override func setUp()
    {
        super.setUp()
		Logger.set(level: Logger.Level.debug, forName: "SQL.SQLite3")

        var dbBytes = try! dbFile.signedBytes(encoding: String.Encoding.utf8)
        dbBytes.append(0)
        unlink(&dbBytes)
        testConnection = SQLLite3Connection(fileName: dbFile)
        try! testConnection.open()
        let createStatement = SQL.Statement.create(
            table: TableName("foo"),
            columns: [ Column(name: "id"  , sqlType: .integer, constraints: [.primaryKey(autoIncrement: true)])
                , Column(name: "name", sqlType: .varChar(20), constraints: [])
                , Column(name: "age" , sqlType: .integer, constraints: [])],
            constraints: [])
		let preparedStatement = try! testConnection.prepare(statement: createStatement)
        try! preparedStatement.execute()
        preparedStatement.finalize()

        let createBar = SQL.Statement.create(
            table: TableName("bar"),
			columns: [ Column(name: "id"  , sqlType: .integer, constraints: [.primaryKey(autoIncrement: true)])
				, Column(name: "name", sqlType: .varChar(20), constraints: [])
                , Column(name: "fooId" , sqlType: .integer, constraints: [])],
            constraints: [])
		let preparedBar = try! testConnection.prepare(statement: createBar)
        try! preparedBar.execute()
        preparedBar.finalize()
    }

    override func tearDown()
    {
        testConnection.close()
        super.tearDown()
    }

    func testCreateTable()
    {
        let testDbName = "testCreateTable.sqlite3"
        var tableBytes = try! testDbName.signedBytes(encoding: String.Encoding.utf8)
        tableBytes.append(0)
        unlink(&tableBytes)
        let myConnection: SQL.Connection = SQLLite3Connection(fileName: testDbName)
        do
        {
            try myConnection.open()
            defer { myConnection.close() }

			var foo = SQL.Table("foo")

            foo.connection = myConnection
			try foo.create(columns: [ Column(name: "id", sqlType: .integer, constraints: [.primaryKey(autoIncrement: true)])
				, Column(name: "name", sqlType: .varChar(20), constraints: [])],
                       constraints: [])
			let tableExists = try myConnection.has(table: foo.name)
			XCTAssert(tableExists, "Failed to create table")
        }
        catch
        {
            XCTFail("Failed with error \(error)")
        }

    }

    func testInsert()
    {
		var foo = SQL.Table("foo")
        do
        {
			foo.connection = testConnection
            try foo.insert(columnValues: ["name" : SQLValue.varChar("bar")])
        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }
    }

    func testSelect()
    {
    	insertFooRow(name: "foo", age: 10)
        insertFooRow(name: "bar", age: 10)
        insertFooRow(name: "baz", age: 20)
        insertFooRow(name: "baz", age: 30)
        var foo = Table("foo")
        do
        {
            foo.connection = testConnection
            var rowCount = 0
			try foo.select(
				columns: ["id", "name"],
				whereClause: nil,
				conversions: [:])
            {
				result in
                log.info("Result is \(result)")
                rowCount++
            }
            XCTAssert(rowCount == 4, "Wrong number of rows")

        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }

    }


    func testSelectWithJoin()
    {
        insertFooRow(name: "foo", age: 10)
        insertFooRow(name: "bar", age: 10)
        insertFooRow(name: "baz", age: 20)
        insertFooRow(name: "baz", age: 30)
        insertBarRow(name: "A bar", foo: 1)
        insertBarRow(name: "Another bar", foo: 1)
        let foo = Table("foo")
        let bar = Table("bar")
		var join = foo.join(bar, leftTable: foo, leftColumn: "id", rightColumn: "fooId")
		join.connection = testConnection

        do
        {
            var rowCount = 0
			try join.select(columns: ["foo.id", "bar.id", "foo.name", "bar.name"], whereClause: nil, conversions: [:])
            {
				row in
                if let resultId = row["bar.id"], let resultName = row["bar.name"]
                {
                    log.info("id = \(resultId), name = '\(resultName)'")
                }
                else
                {
                    XCTFail("Row did not contain the expected columns \(row)")
                }
                rowCount++
            }

            XCTAssert(rowCount == 2, "Wrong number of rows")

        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }

    }

    func testExplicitTransactionThatWorks()
    {
        insertFooRow(name: "foo", age: 10)
        insertFooRow(name: "bar", age: 10)
        insertFooRow(name: "baz", age: 20)
        insertFooRow(name: "baz", age: 30)
        insertBarRow(name: "A bar", foo: 1)
        insertBarRow(name: "Another bar", foo: 1)

        let whereClause = "age" >= SQLValue.integer(20)
        var foo = Table("foo")
        foo.connection = testConnection

        do
        {
            try testConnection.transaction
            {
                let updateCount = try foo.update(columnValues: ["age" : SQLValue.integer(15)], whereClause: whereClause)
				XCTAssert(updateCount == 2)
            }
        }
        catch
        {
            XCTFail("Threw \(error)")
            return
        }
		try! foo.select(columns: ["COUNT(*)"], whereClause: whereClause, conversions: [:])
        {
            result in
            guard let number = result["COUNT(*)"]
            else
            {
                fatalError("Failed to get count")
            }
            XCTAssert(number.asInt! == 0, "The rollback failed")
        }
    }

    func testExplicitTransactionThatFails()
    {
        insertFooRow(name: "foo", age: 10)
        insertFooRow(name: "bar", age: 10)
        insertFooRow(name: "baz", age: 20)
        insertFooRow(name: "baz", age: 30)
        insertBarRow(name: "A bar", foo: 1)
        insertBarRow(name: "Another bar", foo: 1)

        let whereClause = "age" >= SQLValue.integer(20)
        var foo = Table("foo")
        foo.connection = testConnection

        do
        {
            try testConnection.transaction
            {
                let updateCount = try foo.update(columnValues: ["age" : SQLValue.integer(15)], whereClause: whereClause)
				XCTAssert(updateCount == 2)
                throw SQL.Error.implementation(explanation: "Deliberate fail")
            }
            XCTFail("Failed to throw the exception")
        }
        catch
        {
        }
		try! foo.select(columns: ["COUNT(*)"], whereClause: whereClause, conversions: [:])
        {
			result in
            guard let number = result["COUNT(*)"]
            else
            {
                fatalError("Failed to get count")
            }
			XCTAssert(number.asInt! == 2, "The rollback failed")
        }
    }


    func testSelectWithWhere()
    {
        insertFooRow(name: "foo", age: 10)
        insertFooRow(name: "bar", age: 10)
        insertFooRow(name: "baz", age: 20)
        insertFooRow(name: "baz", age: 30)
        var foo = Table("foo")
        foo.connection = testConnection
        do
        {
            var count = 0
			try foo.select(columns: ["id", "name", "age"], whereClause: "age" >= SQLValue.integer(20), conversions: [:])
            {
				row in
                if let resultId = row["id"], let resultName = row["name"], let age = row["age"]
                {
                    log.info("id = \(resultId), name = '\(resultName)', age = \(age)")
                }
                else
                {
                    XCTFail("Row did not contain the expected columns \(row)")
                }
				count++
            }
            XCTAssert(count == 2, "Wrong number of rows")

        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }
    }


    func testForeignKey()
    {
        let dbFile = "foreignkey.sqlite3"
		var dbBytes = dbFile.cString(using: String.Encoding.utf8)!
        unlink(&dbBytes)
        let myConnection = SQLLite3Connection(fileName: dbFile)
        try! myConnection.open()
        defer { myConnection.close() }
        var foo = Table("foo")
        var bar = Table("bar")
        foo.connection = myConnection
        bar.connection = myConnection

		try! foo.create(columns: [ Column(name: "id"  , sqlType: .integer, constraints: [.primaryKey(autoIncrement: true)])
            				     , Column(name: "name", sqlType: .varChar(20), constraints: [])
            				     , Column(name: "age" , sqlType: .integer, constraints: [])],
                    constraints: [])


        do
        {
            try bar.create(columns: [ Column(name: "id"  , sqlType: .integer, constraints: [.primaryKey(autoIncrement: true)])
                                    , Column(name: "name", sqlType: .varChar(20), constraints: [])
                                    , Column(name: "fooId" , sqlType: .integer, constraints: [])],
                       constraints: [Constraint.foreignKey(columns: ["fooId"], referenceTable: TableName("foo"), foreignColumns: ["id"])])
        }
        catch
        {
            XCTFail("Failed to create table with foreign key, error \(error)")
            return
        }
        // This should fail
        do
        {
            try bar.insert(columnValues: ["name" : .varChar("Name"), "fooId" : .integer(1)])
            XCTFail("Foreign key constraint should stop the insert")
        }
        catch
        {
            //            log.info("Correctly failed insert, error \(error)")
        }
    }

    private func insertFooRow(name: String, age: Int)
    {
        let insert = Statement.insert(table: TableName("foo"), columnNames: ["name", "age"])
        do
        {
			let preparedInsert = try testConnection.prepare(statement: insert)
            defer { preparedInsert.finalize() }
			try preparedInsert.bind(values: [.varChar(name), SQLValue.integer(age)])
            try preparedInsert.execute()
        }
        catch
        {
            fatalError("Failed insert, error \(error)")
        }
    }

    private func insertBarRow(name: String, foo: Int)
    {
        let insert = Statement.insert(table: TableName("bar"), columnNames: ["name", "fooId"])
        do
        {
			let preparedInsert = try testConnection.prepare(statement: insert)
            defer { preparedInsert.finalize() }
			try preparedInsert.bind(values: [.varChar(name), .integer(foo)])
            try preparedInsert.execute()
        }
        catch
        {
            fatalError("Failed insert, error \(error)")
        }
    }

    func testDropTable()
    {
        var foo = Table("foo")
        foo.connection = testConnection
        do
        {
            try foo.drop()
			let fooExists = try testConnection.has(table: foo.name)
            XCTAssert(!fooExists, "Failed to drop the table")
        }
        catch
        {
            fatalError("Failed drop table, error \(error)")
        }
    }


    func testUpdate()
    {
        insertFooRow(name: "foo", age: 10)
        insertFooRow(name: "bar", age: 10)
        insertFooRow(name: "baz", age: 20)
        insertFooRow(name: "baz", age: 30)
        var foo = Table("foo")
        foo.connection = testConnection
        do
        {
            let updateCount = try foo.update(columnValues: ["name" : .varChar("Updated"), "age" :  .integer(100)],
                      						whereClause: "age" >= SQLValue.integer(20))
			XCTAssert(updateCount == 2)
			try foo.select(columns: ["COUNT(*)"], whereClause: "age" == .integer(100), conversions: [:])
            {
				result in
				guard let count = result["COUNT(*)"]
                else
                {
                    fatalError("Failed to get count from result \(result)")
                }
                XCTAssert(count.asInt == 2, "Invalid count")
            }
        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }

    }

    func testDelete()
    {
        insertFooRow(name: "foo", age: 10)
        insertFooRow(name: "bar", age: 10)
        insertFooRow(name: "baz", age: 20)
        insertFooRow(name: "baz", age: 30)
        var foo = Table("foo")
		foo.connection = testConnection
        do
        {
            try _ = foo.delete(whereClause: "age" < SQLValue.integer(20))
			try foo.select(columns: ["COUNT(*)"], whereClause: nil, conversions: [:])
            {
                result in
                guard let count = result["COUNT(*)"]
                else
                {
                    fatalError("Failed to get count from result \(result)")
                }
                XCTAssert(count.asInt == 2, "Invalid count")
            }
        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }
        
    }
}
