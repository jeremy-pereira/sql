//
//  SQLTests.swift
//  SQLTests
//
//  Created by Jeremy Pereira on 16/10/2015.
//  Copyright © 2015, 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import SQL
import Toolbox

private let dbFile = "test.sqlite3"

private let log = Logger.getLogger("SQLTests.SQLTests")

class SQLTests: XCTestCase
{
    var testConnection: SQL.Connection!

    override func setUp()
    {
        super.setUp()
		Logger.set(level: Logger.Level.debug, forName: "SQL.SQLite3")

        var dbBytes = try! dbFile.signedBytes(encoding: String.Encoding.utf8)
        dbBytes.append(0)
        unlink(&dbBytes)
      	testConnection = SQLLite3Connection(fileName: dbFile)
        try! testConnection.open()
        let createStatement = SQL.Statement.create(
              table: TableName("foo"),
            columns: [ Column(name: "id"      , sqlType: .integer, constraints: [.primaryKey(autoIncrement: true)])
                     , Column(name: "name"    , sqlType: .varChar(20), constraints: [])
                	 , Column(name: "age"     , sqlType: .integer, constraints: [])
                     , Column(name: "birthday", sqlType: .date, constraints: [])],
            constraints: [])
		let preparedStatement = try! testConnection.prepare(statement: createStatement)
        try! preparedStatement.execute()
        preparedStatement.finalize()

        let createBar = SQL.Statement.create(
            table: TableName("bar"),
            columns: [ Column(name: "id"  , sqlType: .integer, constraints: [.primaryKey(autoIncrement: true)])
                     , Column(name: "name", sqlType: .varChar(20), constraints: [])
                     , Column(name: "fooId" , sqlType: .integer, constraints: [])],
            constraints: [])
		let preparedBar = try! testConnection.prepare(statement: createBar)
        try! preparedBar.execute()
        preparedBar.finalize()
    }

    override func tearDown()
    {
        testConnection.close()
        super.tearDown()
    }

    func testSQLEscape()
    {
        XCTAssert("foo'bar".sqlEscaped == "foo''bar", "SQL escape failed")
    }

    func testCreateConnection()
    {
        let myConnection: SQL.Connection = SQLLite3Connection(fileName: "testcreateconnection.sqlite3")
        do
        {
			try myConnection.open()
            XCTAssert(myConnection.isOpen, "The database is not registered as open")
            myConnection.close()
            XCTAssert(!myConnection.isOpen, "The database is still open")
        }
        catch
        {
            XCTFail("Failed with error \(error)")
        }
    }

    func testCreateTable()
    {
        let tableName = "testCreateTable.sqlite3"
        var tableBytes = try! tableName.signedBytes(encoding: String.Encoding.utf8)
        tableBytes.append(0)
        unlink(&tableBytes)
        let myConnection: SQL.Connection = SQLLite3Connection(fileName: tableName)
        do
        {
            try myConnection.open()
            defer { myConnection.close() }
            let createStatement = SQL.Statement.create(
                 table: TableName("foo"),
               columns: [ Column(name: "id", sqlType: SQLType.integer, constraints: [.primaryKey(autoIncrement: true)])
                        , Column(name: "name", sqlType: .varChar(20), constraints: [])],
                constraints: [])
			let preparedStatement = try myConnection.prepare(statement: createStatement)
            try preparedStatement.execute()
			preparedStatement.finalize()
        }
        catch
        {
            XCTFail("Failed with error \(error)")
        }

    }

    func testInsert()
    {
		let insert = Statement.insert(table: TableName("foo"), columnNames: ["name"])
        do
        {
			let preparedInsert = try testConnection.prepare(statement: insert)
            defer { preparedInsert.finalize() }
			try preparedInsert.bind(values: [SQLValue.varChar("bar")])
            try preparedInsert.execute()
        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }
    }

    func testReset()
    {
        let insert = Statement.insert(table: TableName("foo"), columnNames: ["name"])
        do
        {
			try testConnection.with(statement: insert)
            {
                (stmt) -> () in
				try stmt.bind(values: [SQLValue.varChar("bar")])
                try stmt.execute()
                try stmt.reset()
				try stmt.bind(values: [SQLValue.varChar("baz")])
                try stmt.execute()
            }
        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }
    }
    
    func testGroupBy()
    {
        insertFooRow(name: "foo", age: 10, birthday: now())
        insertFooRow(name: "bar", age: 10, birthday: now())
        insertFooRow(name: "baz", age: 20, birthday: now())
        insertFooRow(name: "baz", age: 30, birthday: now())
		let select = Statement.select(columnNames: ["name", "SUM(age)"],
                                      		 from: TableName("foo"),
										  groupBy: ["name"])
        do
        {
			let preparedSelect = try testConnection.prepare(statement: select)
            defer { preparedSelect.finalize() }

            try preparedSelect.execute()

        	let resultSet = preparedSelect.resultSet

            var rowCount = 0

            while let row = try resultSet.next()
            {
				if let totalAge = row["SUM(age)"], let resultName = row["name"]
                {
                    log.info("id = \(totalAge), name = '\(resultName)'")
					XCTAssert((totalAge.asInt! == 50 && resultName.asString! == "baz")
						|| (totalAge.asInt! == 10 && resultName.asString! != "baz"))
                }
                else
                {
                    XCTFail("Row did not contain the expected columns \(row)")
                }
				rowCount += 1
            }

            XCTAssert(rowCount == 3, "Wrong number of rows")

        }
		catch
        {
            XCTFail("Failed, error \(error)")
        }
        
    }

    func testSelect()
    {
        insertFooRow(name: "foo", age: 10, birthday: now())
        insertFooRow(name: "bar", age: 10, birthday: now())
        insertFooRow(name: "baz", age: 20, birthday: now())
        insertFooRow(name: "baz", age: 30, birthday: now())
        let select = Statement.select(columnNames: ["id", "name"],
                                      		 from: TableName("foo"))
        do
        {
			let preparedSelect = try testConnection.prepare(statement: select)
            defer { preparedSelect.finalize() }

            try preparedSelect.execute()

        	let resultSet = preparedSelect.resultSet

            var rowCount = 0

            while let row = try resultSet.next()
            {
				if let resultId = row["id"], let resultName = row["name"]
                {
                    log.info("id = \(resultId), name = '\(resultName)'")
                }
                else
                {
                    XCTFail("Row did not contain the expected columns \(row)")
                }
				rowCount += 1
            }

            XCTAssert(rowCount == 4, "Wrong number of rows")

        }
		catch
        {
            XCTFail("Failed, error \(error)")
        }
    }


    func testSelectWithPreparedWith()
    {
        insertFooRow(name: "foo", age: 10, birthday: now())
        insertFooRow(name: "bar", age: 10, birthday: now())
        insertFooRow(name: "baz", age: 20, birthday: now())
        insertFooRow(name: "baz", age: 30, birthday: now())
        let select = Statement.select(columnNames: ["id", "name"],
                                      		 from: TableName("foo"))
        do
        {
			var rowCount = 0

			try testConnection.with(statement: select)
			{
				try $0.execute()
				let resultSet = $0.resultSet
				while let row = try resultSet.next()
				{
					if let resultId = row["id"], let resultName = row["name"]
					{
						log.info("id = \(resultId), name = '\(resultName)'")
					}
					else
					{
						XCTFail("Row did not contain the expected columns \(row)")
					}
					rowCount += 1
				}
			}
            XCTAssert(rowCount == 4, "Wrong number of rows")
        }
		catch
        {
            XCTFail("Failed, error \(error)")
        }
    }

    func testSelectWithPreparedWithReturn()
    {
        insertFooRow(name: "foo", age: 10, birthday: now())
        insertFooRow(name: "bar", age: 10, birthday: now())
        insertFooRow(name: "baz", age: 20, birthday: now())
        insertFooRow(name: "baz", age: 30, birthday: now())
        let select = Statement.select(columnNames: ["id", "name"],
                                      		 from: TableName("foo"))
        do
        {
			let rowCount = try testConnection.with(statement: select)
			{
				(p) -> Int in
				try p.execute()
				var rowCount = 0
				let resultSet = p.resultSet
				while let row = try resultSet.next()
				{
					if let resultId = row["id"], let resultName = row["name"]
					{
						log.info("id = \(resultId), name = '\(resultName)'")
					}
					else
					{
						XCTFail("Row did not contain the expected columns \(row)")
					}
					rowCount += 1
				}
				return rowCount
			}
            XCTAssert(rowCount == 4, "Wrong number of rows")
        }
		catch
        {
            XCTFail("Failed, error \(error)")
        }
    }


    func testSelectDate()
    {
        let currentDate = now()
        insertFooRow(name: "foo", age: 10, birthday: currentDate)
        let select = Statement.select(columnNames: ["id", "birthday"],
                                      		 from: TableName("foo"),
                                      conversions: [ "birthday" : { try $0.toSQLDate() } ])
        do
        {
			let preparedSelect = try testConnection.prepare(statement: select)
            defer { preparedSelect.finalize() }

            try preparedSelect.execute()

            let resultSet = preparedSelect.resultSet

            var rowCount = 0

            while let row = try resultSet.next()
            {
				if let resultId = row["id"], let resultDate = row["birthday"]
                {
                    if case SQLValue.date(let actualDate) = resultDate
                    {
                        log.info("id = \(resultId), birthday = '\(actualDate)'")
                    }
                    else
                    {
                    	XCTFail("Invalid date \(resultDate)")
                    }
                }
                else
                {
                    XCTFail("Row did not contain the expected columns \(row)")
                }
                rowCount += 1
            }

            XCTAssert(rowCount == 1, "Wrong number of rows")

        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }
        
    }


    func testSelectWithJoin()
    {
        insertFooRow(name: "foo", age: 10, birthday: now())
        insertFooRow(name: "bar", age: 10, birthday: now())
        insertFooRow(name: "baz", age: 20, birthday: now())
        insertFooRow(name: "baz", age: 30, birthday: now())
        insertBarRow(name: "A bar", foo: 1)
        insertBarRow(name: "Another bar", foo: 1)
		let fromClause = TableName("foo").join(right: TableName("bar"), leftColumn: "foo.id", op: SQLRelationalOp.eq, rightColumn: "bar.fooId")

        let select = Statement.select(columnNames: ["foo.id", "bar.id", "foo.name", "bar.name"], from: fromClause)
        do
        {
			let preparedSelect = try testConnection.prepare(statement: select)
            defer { preparedSelect.finalize() }

            try preparedSelect.execute()

            let resultSet = preparedSelect.resultSet

            var rowCount = 0

            while let row = try resultSet.next()
            {
				if let resultId = row["bar.id"], let resultName = row["bar.name"]
                {
                    log.info("id = \(resultId), name = '\(resultName)'")
                }
                else
                {
                    XCTFail("Row did not contain the expected columns \(row)")
                }
                rowCount += 1
            }

            XCTAssert(rowCount == 2, "Wrong number of rows")

        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }
        
    }

    func testExplicitTransactionThatWorks()
    {
        insertFooRow(name: "foo", age: 10, birthday: now())
        insertFooRow(name: "bar", age: 10, birthday: now())
        insertFooRow(name: "baz", age: 20, birthday: now())
        insertFooRow(name: "baz", age: 30, birthday: now())
        insertBarRow(name: "A bar", foo: 1)
        insertBarRow(name: "Another bar", foo: 1)

        let whereClause = SQLBooleanExpression.relation("age", .geq, SQLValue.integer(20))

        let update = Statement.update(table: TableName("foo"), columns: ["age"], whereClause: whereClause)
		let preparedUpdate = try! testConnection.prepare(statement: update)
        defer { preparedUpdate.finalize() }
		try! preparedUpdate.bind(values: [SQLValue.integer(15)])
        do
        {
            try testConnection.transaction
            {
                try preparedUpdate.execute()
            }

        }
        catch
        {
            XCTFail("Failed to do update")
        }
		let select = Statement.select(columnNames: ["COUNT(*)"], from: TableName("foo"), where: whereClause)
		let preparedSelect = try! testConnection.prepare(statement: select)
        defer { preparedSelect.finalize() }

        try! preparedSelect.execute()
        guard let result = try? preparedSelect.resultSet.next()
		else
		{
			fatalError("Failed to get count")
		}
		guard let number = result["COUNT(*)"]
        else
        {
            fatalError("Failed to get count")
        }
        switch number
        {
        case SQLValue.integer(let count):
            XCTAssert(count == 0, "Wrong count \(count)")
        default:
            XCTFail("Expected count to be a number")
        }

    }

    func testExplicitTransactionThatFails()
    {
        insertFooRow(name: "foo", age: 10, birthday: now())
        insertFooRow(name: "bar", age: 10, birthday: now())
        insertFooRow(name: "baz", age: 20, birthday: now())
        insertFooRow(name: "baz", age: 30, birthday: now())
        insertBarRow(name: "A bar", foo: 1)
        insertBarRow(name: "Another bar", foo: 1)

        let whereClause = SQLBooleanExpression.relation("age", .geq, .integer(20))

        let update = Statement.update(table: TableName("foo"), columns: ["age"], whereClause: whereClause)
		let preparedUpdate = try! testConnection.prepare(statement: update)
        defer { preparedUpdate.finalize() }
		try! preparedUpdate.bind(values: [SQLValue.integer(15)])
        do
        {
            try testConnection.transaction
            {
                try preparedUpdate.execute()
                throw SQL.Error.implementation(explanation: "Deliberate fail")
            }
            XCTFail("Failed to throw the exception")
        }
        catch
        {
        }
		let select = Statement.select(columnNames: ["COUNT(*)"], from: TableName("foo"), where: whereClause)
		let preparedSelect = try! testConnection.prepare(statement: select)
        defer { preparedSelect.finalize() }

        try! preparedSelect.execute()
        guard let result = try? preparedSelect.resultSet.next()
            else
        {
            fatalError("Failed to get count")
        }
        guard let number = result["COUNT(*)"]
            else
        {
            fatalError("Failed to get count")
        }
        switch number
        {
        case SQLValue.integer(let count):
            XCTAssert(count == 2)
        default:
            XCTFail("Expected count to be a number")
        }
        
    }


    func testSelectWithWhere()
    {
        insertFooRow(name: "foo", age: 10, birthday: now())
        insertFooRow(name: "bar", age: 10, birthday: now())
        insertFooRow(name: "baz", age: 20, birthday: now())
        insertFooRow(name: "baz", age: 30, birthday: now())
        let select = Statement.select(columnNames: ["id", "name", "age"],
            								 from: TableName("foo"),
											where: .relation("age", .geq, .integer(20)))
        do
        {
			let preparedSelect = try testConnection.prepare(statement: select)
            defer { preparedSelect.finalize() }

            try preparedSelect.execute()

            let resultSet = preparedSelect.resultSet

            var rowCount = 0

            while let row = try resultSet.next()
            {
				if let resultId = row["id"], let resultName = row["name"], let age = row["age"]
                {
                    log.info("id = \(resultId), name = '\(resultName)', age = \(age)")
                }
                else
                {
                    XCTFail("Row did not contain the expected columns \(row)")
                }
                rowCount += 1
            }

            XCTAssert(rowCount == 2, "Wrong number of rows")

        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }
        
    }

    func testSelectWithWhereBinding()
    {
        insertFooRow(name: "foo", age: 10, birthday: now())
        insertFooRow(name: "bar", age: 10, birthday: now())
        insertFooRow(name: "baz", age: 20, birthday: now())
        insertFooRow(name: "baz", age: 30, birthday: now())
        let select = Statement.select(columnNames: ["id", "name", "age"],
            								 from: TableName("foo"),
                                            where: .relation("age", .geq, .placeHolder))
        do
        {
			let preparedSelect = try testConnection.prepare(statement: select)
            defer { preparedSelect.finalize() }

			try preparedSelect.bind(values: [SQLValue.integer(30)])
            try preparedSelect.execute()

            let resultSet = preparedSelect.resultSet

            var rowCount = 0

            while let row = try resultSet.next()
            {
				if let resultId = row["id"], let resultName = row["name"], let age = row["age"]
                {
                    log.info("id = \(resultId), name = '\(resultName)', age = \(age)")
                }
                else
                {
                    XCTFail("Row did not contain the expected columns \(row)")
                }
                rowCount += 1
            }

            XCTAssert(rowCount == 1, "Wrong number of rows")

        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }
        
    }

    func testForeignKey()
    {
        let dbFile = "foreignkey.sqlite3"
        var dbBytes = try! dbFile.signedBytes(encoding: String.Encoding.utf8)
        dbBytes.append(0)
        unlink(&dbBytes)
        let myConnection = SQLLite3Connection(fileName: dbFile)
        try! myConnection.open()
        defer { myConnection.close() }
        let createStatement = SQL.Statement.create(
              table: TableName("foo"),
            columns: [ Column(name: "id"  , sqlType: .integer, constraints: [.primaryKey(autoIncrement: true)])
                     , Column(name: "name", sqlType: .varChar(20), constraints: [])
                     , Column(name: "age" , sqlType: .integer, constraints: [])],
            constraints: [])
		let preparedStatement = try! myConnection.prepare(statement: createStatement)
        try! preparedStatement.execute()
        preparedStatement.finalize()

        let createBar = SQL.Statement.create(
                  table: TableName("bar"),
				  columns: [ Column(name: "id"  , sqlType: .integer, constraints: [.primaryKey(autoIncrement: true)])
                         , Column(name: "name", sqlType: .varChar(20), constraints: [])
                         , Column(name: "fooId" , sqlType: .integer, constraints: [])],
				constraints: [.foreignKey(columns: ["fooId"], referenceTable: TableName("foo"), foreignColumns: ["id"])]
        )
        do
        {
			let preparedBar = try myConnection.prepare(statement: createBar)
            defer { preparedBar.finalize() }
            try preparedBar.execute()
        }
        catch
        {
            XCTFail("Failed to create table with foreign key, error \(error)")
            return
        }
        // This should fail
        let insert = Statement.insert(table: TableName("bar"), columnNames: ["name", "fooId"])
        do
        {
			let preparedInsert = try myConnection.prepare(statement: insert)
            defer { preparedInsert.finalize() }
			try preparedInsert.bind(values: [.varChar(name), SQLValue.integer(1)])
            try preparedInsert.execute()
            XCTFail("Foreign key constraint should stop the insert")
        }
        catch
        {
            //            log.info("Correctly failed insert, error \(error)")
        }
    }

    private func insertFooRow(name: String, age: Int, birthday: Date)
    {
        let insert = Statement.insert(table: TableName("foo"), columnNames: ["name", "age", "birthday"])
        do
        {
			let preparedInsert = try testConnection.prepare(statement: insert)
            defer { preparedInsert.finalize() }
			try preparedInsert.bind(values: [.varChar(name), SQLValue.integer(age), .date(birthday)])
            try preparedInsert.execute()
        }
        catch
        {
            fatalError("Failed insert, error \(error)")
        }
    }

    private func insertBarRow(name: String, foo: Int)
    {
        let insert = Statement.insert(table: TableName("bar"), columnNames: ["name", "fooId"])
        do
        {
			let preparedInsert = try testConnection.prepare(statement: insert)
            defer { preparedInsert.finalize() }
			try preparedInsert.bind(values: [.varChar(name), .integer(foo)])
            try preparedInsert.execute()
        }
        catch
        {
            fatalError("Failed insert, error \(error)")
        }
    }

    func testDropTable()
    {
        let drop = Statement.dropTable(TableName("foo"))
        do
        {
			let preparedDrop = try testConnection.prepare(statement: drop)
            defer { preparedDrop.finalize() }
            try preparedDrop.execute()
        }
        catch
        {
            fatalError("Failed drop table, error \(error)")
        }
    }


    func testUpdate()
    {
        insertFooRow(name: "foo", age: 10, birthday: now())
        insertFooRow(name: "bar", age: 10, birthday: now())
        insertFooRow(name: "baz", age: 20, birthday: now())
        insertFooRow(name: "baz", age: 30, birthday: now())
        let update = Statement.update(table: TableName("foo"), columns: ["name", "age"],
                                whereClause: "age" >= .integer(20))
        do
        {
			let preparedUpdate = try testConnection.prepare(statement: update)
            defer { preparedUpdate.finalize() }
			try preparedUpdate.bind(values: [.varChar("Updated"), .integer(100)])
            try preparedUpdate.execute()

        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }
        
    }

    func testDelete()
    {
        insertFooRow(name: "foo", age: 10, birthday: now())
        insertFooRow(name: "bar", age: 10, birthday: now())
        insertFooRow(name: "baz", age: 20, birthday: now())
        insertFooRow(name: "baz", age: 30, birthday: now())
        let delete = Statement.delete(from: TableName("foo"),
                               whereClause: "age" >= SQLValue.integer(20))
        do
        {
			let preparedDelete = try testConnection.prepare(statement: delete)
            defer { preparedDelete.finalize() }
            try preparedDelete.execute()

        }
        catch
        {
            XCTFail("Failed, error \(error)")
        }

    }

    func testTableExists()
    {
		XCTAssert(try! testConnection.has(table: TableName("foo")), "foo does not exist")
		XCTAssert(try! !testConnection.has(table: TableName("banana")), "banana does exist")
    }

    static var allTests = 
    [
        ("testDropTable", testDropTable),
    ]
}
