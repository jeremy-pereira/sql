#!/bin/sh

author="Jeremy Pereira"
copyright="Copyright © 2020 Jeremy Pereira"

makeADoc() {

	module=$1
	
	echo "Documenting $module"
	
	readme="$module/README.md"
	
	mkdir -p docs/$module
	jazzy -a "$author" -m $module --readme "$readme" --copyright "$copyright" -o "docs/$module" \
		  --swift-build-tool spm \
		  --build-tool-arguments -Xswiftc,-swift-version,-Xswiftc,5
}

if [ x"$1" != "x" ] 
then
	makeADoc "$1"
else
	makeADoc SQL
fi
